import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '@app/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

import {
  NbActionsModule, NbLayoutModule
} from '@nebular/theme';

@NgModule({
  imports: [CommonModule, TranslateModule, 
    CoreModule, HomeRoutingModule, NbActionsModule, NbLayoutModule],
  declarations: [HomeComponent],
})
export class HomeModule {}
