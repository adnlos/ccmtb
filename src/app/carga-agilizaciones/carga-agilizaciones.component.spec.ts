import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargaAgilizacionesComponent } from './carga-agilizaciones.component';

describe('CargaAgilizacionesComponent', () => {
  let component: CargaAgilizacionesComponent;
  let fixture: ComponentFixture<CargaAgilizacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargaAgilizacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargaAgilizacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
