import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CargaAgilizacionesRoutingModule } from "./carga-agilizaciones-routing.module";
import { CargaAgilizacionesComponent } from "./carga-agilizaciones.component";
import { ReactiveFormsModule } from '@angular/forms';
import { EmpresaService } from "./empresa.service";
import { FormsModule } from '@angular/forms';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { AuthenticationService } from '../core/authentication/authentication.service'



import {
  NbThemeModule,
  NbCardModule,
  NbLayoutModule,
  NbSelectModule,
  NbInputModule,
  NbButtonModule
} from '@nebular/theme';

@NgModule({
  declarations: [CargaAgilizacionesComponent],
  imports: [
    NbThemeModule,
    CommonModule,
    CargaAgilizacionesRoutingModule,
    NbCardModule,
    NbLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    AngularFileUploaderModule
  ],
  providers:[EmpresaService, AuthenticationService]
})
export class CargaAgilizacionesModule { }
