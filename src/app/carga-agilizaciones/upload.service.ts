import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from  '@angular/common/http';
import { map } from  'rxjs/operators';

import { environment } from '@env/environment';


@Injectable({
  providedIn: 'root'
})
export class UploadService {
  baseUrlPrivate = environment.serverUrlPrivate;


  constructor(private httpClient: HttpClient) { }


  public upload(data, userId) {
    let uploadURL = `${this.baseUrlPrivate}/upload/${userId}`;

    return this.httpClient.post<any>(uploadURL, data, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map((event) => {

      switch (event.type) {

        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    })
    );
  }
}
