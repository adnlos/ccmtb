import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable()
export class EmpresaService {

  baseUrlPrivate = environment.serverUrlPrivate;
  constructor(private httpClient: HttpClient) { }
  
  getEmpresas(): Observable<any>{
    return this.httpClient.get<any>(this.baseUrlPrivate + 'empresas');
  }

}
