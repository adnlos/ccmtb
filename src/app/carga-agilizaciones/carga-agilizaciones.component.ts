import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { UploadService } from  './upload.service';
import { EmpresaService } from "./empresa.service";
import { environment } from '@env/environment';
import { AngularFileUploaderComponent } from "angular-file-uploader";

import { AuthenticationService } from '../core/authentication/authentication.service'


@Component({
  selector: 'app-carga-agilizaciones',
  templateUrl: './carga-agilizaciones.component.html',
  styleUrls: ['./carga-agilizaciones.component.scss']
})
export class CargaAgilizacionesComponent implements OnInit {
  
  baseUrlPrivate = environment.serverUrlPrivate;

  formUpload: FormGroup;
  loadingUpload = false;
  errorUpload = false;
  errorFileReq = 'Debe Seleccionar un archivo';
  empresas: any;
  errorUploadRequest = false;
  successUploadRequest = false;
  

  errorMsgResponse: string;

  

  @ViewChild('fileUpload1')
  private fileUpload1:  AngularFileUploaderComponent;

  afuConfig = {
    multiple: false,
    formatsAllowed: ".xlsx, .xls",
    maxSize: "1",
    uploadAPI:  {
      url:this.baseUrlPrivate+"agilizacion/upload/",
      headers: {
       "X-Auth-Token" : this.authenticationService.credentials.token
      }
    },
    hideProgressBar: false,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      selectFileBtn: 'Selecciona Archivo',
      resetBtn: 'x',
      uploadBtn: 'Subir',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Adjuntar archivo...',
      afterUploadMsg_success: 'Excito, archivo cargado!',
      afterUploadMsg_error: 'Error algo al cargar archivo!'
    }
};


constructor(private formBuilder: FormBuilder, 
  private uploadService: UploadService, 
  private empresaService: EmpresaService, 
  private authenticationService: AuthenticationService) { }

ngOnInit() {
  this.formUpload = this.formBuilder.group({
    empresaId:["", Validators.required],
  });
  
  this.empresaService.getEmpresas().subscribe(data => {
    this.empresas = data;
  });

}

  get empresa(){
    return this.formUpload.get("empresaId");
  }

  onSubmit() {
    this.loadingUpload = true;

    if (this.formUpload.invalid) {
      return;
    }

    let urlarchivo = this.getUrlArchivo(this.formUpload.value.empresaId);
    this.fileUpload1.uploadAPI = urlarchivo; 

    if (this.fileUpload1.selectedFiles.length !== 0) {
      this.fileUpload1.uploadFiles();
    }else{
      this.fileUpload1.resetFileUpload();
      this.errorUpload = true;
      return;
    }
  }

  fileUploadChanged(newObj) {
    this.errorUpload = false;
    this.loadingUpload = false;
  }

  private getUrlArchivo(idEmpresa: any) {
    return this.baseUrlPrivate+"agilizacion/upload/"+idEmpresa;
  }

  respUpload(eventRequest){
    let xhr = new XMLHttpRequest();
    xhr = eventRequest;
    let progress = 0;

    console.log(xhr.response);
    let response = JSON.parse(xhr.response);

    console.log(response);
    if (response.statusCode === 404) {
      console.log("404");

      this.errorUploadRequest = true;
      this.errorMsgResponse = response.message; 
    }
    else if (response.statusCode === 200) {
      this.errorUploadRequest = false;
      this.successUploadRequest = true;
      this.errorMsgResponse = response.message; 
    }else{
      console.log("otro error");
    }
  }
}
