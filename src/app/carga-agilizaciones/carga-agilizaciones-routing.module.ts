import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CargaAgilizacionesComponent } from './carga-agilizaciones.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'carga-agilizaciones', component: CargaAgilizacionesComponent }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CargaAgilizacionesRoutingModule { }
