import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { ConsultaRemesaService } from "./consulta-remesa.service";
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-consulta-remesa',
  templateUrl: './consulta-remesa.component.html',
  styleUrls: ['./consulta-remesa.component.scss']
})
export class ConsultaRemesaComponent implements OnInit {

  formConsultaRemesa: FormGroup;
  empresas: any;
  loading = false;
  displayTable = false;
  success = false;
  error = false;
  errorMsg = '';
  errorMsgTable = 'Debe seleccionar un elemento';
  dataTable: any;
  selectRemesasTable: any | undefined;
  date = new Date();
  blob: any


  loadingDescarga = false;
  isSelectedTable = false;

  settings = {
    selectMode: 'multi',
    mode: 'external',
    hideSubHeader: true,
    editable: false,
    actions:{
      add:false,
      delete:false,
      edit:false,
      select: true,
    },
    columns: {
      folioRemesa: {
        title: 'Clave remesa',
        class: 'center'
      },
      fechaEmision: {
        title: 'Fecha de emisión'
      },
      horaEmision: {
        title: 'Hora de emisión'
      },
      estadoRemesa: {
        title: 'Estado de emisión'
      },
      cantDocumento: {
        title: 'Total Documentos'
      }
    }
  };

  onUserRowSelect(event) {
    this.selectRemesasTable = event;
    this.isSelectedTable = true;
    this.loadingDescarga = false;
  }

  constructor(private formBuilder: FormBuilder, 
    private consultaRemesaService: ConsultaRemesaService, private nbToastrService: NbToastrService) { }
  
  ngOnInit() {
    this.formConsultaRemesa = this.formBuilder.group({
      empresaId:["", Validators.required],
      folioRemesa:"",
      fechaFin: ""
    });

    this.consultaRemesaService.getEmpresas().subscribe(data => {
      this.empresas = data;
    });
  }

  get empresa(){
    return this.formConsultaRemesa.get("empresaId");
  }

  get folioRemesa(){
    return this.formConsultaRemesa.get("folioRemesa");
  }

  get fechaFin(){
    return this.formConsultaRemesa.get("fechaFin");
  }

  btnConsultaRemesas() {
    this.loading = true;
    this.error = false;
    this.success = false;
    this.errorMsg= '';


    if (this.formConsultaRemesa.invalid) {
      return;
    }
    this.consultaRemesaService.getRemesasDatos(this.formConsultaRemesa.value).subscribe(
      data => {
        this.success = true;
        this.error = false;
        this.loading = false;
        this.displayTable = true;
        this.dataTable = data;
      },
      error => {
        this.error = true;
        this.displayTable = false;
        this.dataTable = [];
        this.errorMsg = error.error.message;
      });
  }

  btnDescargaConsultaRemesas ()  {

    this.loadingDescarga = true;
    
    if(this.selectRemesasTable === undefined || this.selectRemesasTable.selected == 0){
      this.isSelectedTable = true;      
    }else{
      this.isSelectedTable = false;

      this.selectRemesasTable.selected.forEach(element => {
        
        this.consultaRemesaService.descargaRemesa(this.formConsultaRemesa.value, element ).subscribe(
          data => {
            this.blob = new Blob([data], {type: 'txt/plain'});
            var downloadURL = window.URL.createObjectURL(data);
            var link = document.createElement('a');
            link.href = downloadURL;
            link.download = element.folioRemesa + ".txt";
            link.click();
          },
          error => {
            this.nbToastrService.danger('Error', 'Error descarga remesa');
            // this.nbToastrService.show('Error', 'Error descarga remesa');
            // this.error = true;
            // // //TODO verificar undefined en mensaje
            // this.errorMsg = error.error.message;
          }
        );

      });
    }
  }


}
