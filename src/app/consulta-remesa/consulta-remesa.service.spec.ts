import { TestBed } from '@angular/core/testing';

import { ConsultaRemesaService } from './consulta-remesa.service';

describe('ConsultaRemesaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConsultaRemesaService = TestBed.get(ConsultaRemesaService);
    expect(service).toBeTruthy();
  });
});
