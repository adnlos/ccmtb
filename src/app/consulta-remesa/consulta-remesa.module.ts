import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ConsultaRemesaRouting } from "./consulta-remesa-routing.module";
import { ConsultaRemesaComponent } from "./consulta-remesa.component";
import { ConsultaRemesaService } from "./consulta-remesa.service";
import { Ng2SmartTableModule } from 'ng2-smart-table';


import {
  NbThemeModule,
  NbCardModule,
  NbLayoutModule,
  NbSelectModule,
  NbInputModule,
  NbButtonModule,
  NbAlertModule,
  NbCalendarModule,
  NbDatepickerModule,
  NbToastrService
} from '@nebular/theme';



@NgModule({
  declarations: [ConsultaRemesaComponent ],
  imports: [
    CommonModule,
    ConsultaRemesaRouting,
    ReactiveFormsModule,
    NbThemeModule,
    NbCardModule,
    NbLayoutModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    Ng2SmartTableModule,
    NbAlertModule,
    NbCalendarModule,
    NbDatepickerModule,
  ],
  providers:[ConsultaRemesaService, NbToastrService]
})
export class ConsultaRemesaModule { }
