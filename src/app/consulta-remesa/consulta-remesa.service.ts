import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';

import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable()
export class ConsultaRemesaService {

  baseUrlPrivate = environment.serverUrlPrivate;
  constructor(private httpClient: HttpClient) { }
  
  getEmpresas(): Observable<any>{
    return this.httpClient.get<any>(this.baseUrlPrivate + 'empresas');
  }

  getRemesasDatos( request: any):Observable<any>{
    let params = new HttpParams();
    params = params.append('empresaId', request.empresaId);

    if (request.folioRemesa !== "") {
      params = params.append("folioRemesa", request.folioRemesa);
    }
    if (request.fechaFin !== "" && request.fechaFin !== null) {
      params = params.append("fecha", this.convert(request.fechaFin));
    }
    console.log(params.toString());
    return this.httpClient.get<any>(this.baseUrlPrivate + 'remesas/consulta/',{params});
  
  }

  private convert(str) {
    var date = new Date(str),
    mnth = ("0" + (date.getMonth()+1)).slice(-2),
    day  = ("0" + date.getDate()).slice(-2);
    return [ day , mnth, date.getFullYear() ].join("/");
  }

  
  descargaRemesa( empresa: any, remesa: any):Observable<any>{
    const  params = new  HttpParams().set('empresaId', empresa.empresaId).set('remesaId', remesa.remesaId);
    const options: {
      headers?: HttpHeaders,
      observe?: 'body',
      params?: HttpParams,
      reportProgress?: boolean,
      responseType: 'blob',
      withCredentials?: boolean
    } = {
      params: params,
      responseType: 'blob'
    };
    return this.httpClient.get(this.baseUrlPrivate + 'remesas/descarga/', options);    
  }
}