import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaRemesaComponent } from './consulta-remesa.component';

describe('ConsultaRemesaComponent', () => {
  let component: ConsultaRemesaComponent;
  let fixture: ComponentFixture<ConsultaRemesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaRemesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaRemesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
