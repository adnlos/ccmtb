import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbMenuModule, 
  NbSidebarModule, NbDatepickerModule, NbToastrModule } from '@nebular/theme';

// import { AngularFileUploaderModule } from "angular-file-uploader";

import { NbDateFnsDateModule } from '@nebular/date-fns';

import * as esLocale from 'date-fns/locale/es';


import { environment } from '@env/environment';
import { CoreModule } from '@app/core';
import { HomeModule } from './home/home.module';
import { ShellModule } from './shell/shell.module';
import { LoginModule } from './login/login.module';
import { EmpleadoModule } from './empleado/empleado.module'
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LayoutService } from './shell/layout.service';
import { Ng2CompleterModule } from "ng2-completer";

import { UsuarioModule } from "./usuario/usuario.module";
import { CargaAgilizacionesModule } from "./carga-agilizaciones/carga-agilizaciones.module";
import { DescargaRemesaModule } from './descarga-remesa/descarga-remesa.module';
import { ConsultaRemesaModule } from './consulta-remesa/consulta-remesa.module';
import { RecepcionRemesaModule } from './recepcion-remesa/recepcion-remesa.module';
import { CierreRemesaParcialModule } from './cierre-remesa-parcial/cierre-remesa-parcial.module';
import { CierreRemesaRealModule } from './cierre-remesa-real/cierre-remesa-real.module';
import { CierreRemesaBovedaModule } from './cierre-remesa-boveda/cierre-remesa-boveda.module';
import { ReenvioRemesaModule } from './reenvio-remesa/reenvio-remesa.module';
import { CargaInventarioModule } from "./carga-inventario/carga-inventario.module";
import { ActualizacionInventarioModule } from "./actualizacion-inventario/actualizacion-inventario.module";



import { ConsultaInventarioComponent } from './consulta-inventario/consulta-inventario.component';
import { ActualizacionInventarioComponent } from './actualizacion-inventario/actualizacion-inventario.component';
import { CargaInventarioComponent } from './carga-inventario/carga-inventario.component';



@NgModule({
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    NgbModule,
    CoreModule,
    ShellModule,
    HomeModule,
    EmpleadoModule,
    LoginModule,
    UsuarioModule,
    DescargaRemesaModule,
    CargaAgilizacionesModule,
    BrowserAnimationsModule,
    ConsultaRemesaModule,
    RecepcionRemesaModule,
    CierreRemesaRealModule,
    CierreRemesaParcialModule,
    CierreRemesaBovedaModule,
    ReenvioRemesaModule,
    CargaInventarioModule,
    ActualizacionInventarioModule,
    Ng2CompleterModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbMenuModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbToastrModule.forRoot(),//toast
    // AngularFileUploaderModule,
    NbDateFnsDateModule.forRoot({
      format: 'MM/DD/YYYY',
      parseOptions: { locale: esLocale, awareOfUnicodeTokens: true },
      formatOptions: { locale: esLocale, awareOfUnicodeTokens: true }
    }),

    AppRoutingModule // must be imported as the last module as it contains the fallback route
  ],
  declarations: [AppComponent],
  providers: [LayoutService],
  bootstrap: [AppComponent]
})
export class AppModule {}
