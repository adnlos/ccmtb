import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReenvioRemesaComponent } from './reenvio-remesa.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'reenvio-remesa', component: ReenvioRemesaComponent }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReenvioRemesaRoutingModule { }
