import { TestBed } from '@angular/core/testing';

import { ReenvioRemesaService } from './reenvio-remesa.service';

describe('ReenvioRemesaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReenvioRemesaService = TestBed.get(ReenvioRemesaService);
    expect(service).toBeTruthy();
  });
});
