import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { ReenvioRemesaRoutingModule } from "./reenvio-remesa-routing.module";
import { ReenvioRemesaComponent } from "./reenvio-remesa.component";
import { ReenvioRemesaService } from "./reenvio-remesa.service";
import { Ng2SmartTableModule } from 'ng2-smart-table';

import {
  NbCardModule,
  NbSelectModule,
  NbInputModule,
  NbButtonModule,
  NbAlertModule,
  NbToastrService
} from '@nebular/theme';

@NgModule({
  declarations: [ReenvioRemesaComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    NbAlertModule,
    ReenvioRemesaRoutingModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
  ],
  providers:[ReenvioRemesaService]
})
export class ReenvioRemesaModule { }
