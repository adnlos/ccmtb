import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReenvioRemesaComponent } from './reenvio-remesa.component';

describe('ReenvioRemesaComponent', () => {
  let component: ReenvioRemesaComponent;
  let fixture: ComponentFixture<ReenvioRemesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReenvioRemesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReenvioRemesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
