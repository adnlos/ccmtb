import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NbLayoutModule, NbCardModule,  
  NbAlertModule, NbInputModule, NbCheckboxModule, NbButtonModule } from '@nebular/theme';


import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, 
    TranslateModule, NgbModule, LoginRoutingModule, 
    NbLayoutModule, NbAlertModule, NbInputModule, 
    NbCheckboxModule, NbCardModule, NbButtonModule],
  declarations: [LoginComponent]
})
export class LoginModule {}
