import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from  '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


import { Logger, AuthenticationService } from '@app/core';

const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  error = false;
  success = false;
  errorMsg: string = '';
  isLoading = false;

  redirectDelay: number = 0;
  strategy: string = '';

  user: any = {};
  rememberMe = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.createForm();
  }

  login() {
    this.error = false;
    this.isLoading = true;
    this.success = false;
    this.errorMsg= '';
    
    if (this.loginForm.invalid) {
      return;
    }

    this.authenticationService
      .login(this.loginForm.value)
      .subscribe(
        credentials => {
          log.debug(`${credentials.username} successfully logged in`);

          this.success = true;
          this.error = false;
          
          this.route.queryParams.subscribe(params =>
            this.router.navigate([params.redirect || '/'], { replaceUrl: true })
          );
        },
        error => {
          console.log(`user error: ${error.error.message}`);
          this.error = true;
          this.errorMsg = error.error.message;
        }
      );
  }


  private createForm(){
    this.loginForm = this.formBuilder.group({
      username:["", Validators.required],
      password: ["", Validators.required],
      remember:[""]
    });
  }
  
  get password(){
    return this.loginForm.get("password");
  }

  get username(){
    return this.loginForm.get("username");
  }
  


}
