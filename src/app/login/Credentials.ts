export interface Credentials {
    username: string;
    token: string;
    roles: { authority: string }[]
    email: string;
}