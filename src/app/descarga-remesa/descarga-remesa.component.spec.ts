import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescargaRemesaComponent } from './descarga-remesa.component';

describe('DescargaRemesaComponent', () => {
  let component: DescargaRemesaComponent;
  let fixture: ComponentFixture<DescargaRemesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescargaRemesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescargaRemesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
