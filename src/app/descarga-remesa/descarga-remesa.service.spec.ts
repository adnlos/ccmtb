import { TestBed } from '@angular/core/testing';

import { DescargaRemesaService } from './descarga-remesa.service';

describe('DescargaRemesaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DescargaRemesaService = TestBed.get(DescargaRemesaService);
    expect(service).toBeTruthy();
  });
});
