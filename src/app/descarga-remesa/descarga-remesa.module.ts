import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DescargaRemesaRoutingModule } from "./descarga-remesa-routing.module";
import { DescargaRemesaComponent } from "./descarga-remesa.component";
import { DescargaRemesaService } from "./descarga-remesa.service";
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ReactiveFormsModule } from '@angular/forms';


import {
  NbThemeModule,
  NbLayoutModule,
  NbButtonModule,
  NbAlertModule,
  NbCardModule,
  NbSelectModule
} from '@nebular/theme';


@NgModule({
  declarations: [DescargaRemesaComponent],
  imports: [
    CommonModule,
    DescargaRemesaRoutingModule,
    NbThemeModule,
    NbLayoutModule,
    NbButtonModule,
    NbAlertModule,
    NbCardModule,
    Ng2SmartTableModule,
    NbSelectModule,
    ReactiveFormsModule
  ],
  providers:[DescargaRemesaService]
})
export class DescargaRemesaModule { }
