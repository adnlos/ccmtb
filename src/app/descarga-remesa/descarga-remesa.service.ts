import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';

import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable()
export class DescargaRemesaService {

  baseUrlPrivate = environment.serverUrlPrivate;
  constructor(private httpClient: HttpClient) { }
  
  getEmpresas(): Observable<any>{
    return this.httpClient.get<any>(this.baseUrlPrivate + 'empresas');
  }
  
  //datos remesas tabla
  getRemesasDatos( request: any):Observable<any>{
    const  params = new  HttpParams().set('empresaId', request.empresaId);
    return this.httpClient.get<any>(this.baseUrlPrivate + 'remesas/descarga/consulta/',{params});
  }

  descargaRemesa( empresa: any, remesa: any):Observable<any>{
    const  params = new  HttpParams().set('empresaId', empresa.empresaId).set('remesaId', remesa.remesaId);
    const options: {
      headers?: HttpHeaders,
      observe?: 'body',
      params?: HttpParams,
      reportProgress?: boolean,
      responseType: 'blob',
      withCredentials?: boolean
    } = {
      // headers: headers,
      params: params,
      responseType: 'blob'
    };
    return this.httpClient.get(this.baseUrlPrivate + 'remesas/descarga/', options);    
  }









}
