import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DescargaRemesaComponent } from './descarga-remesa.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'descarga-remesa', component: DescargaRemesaComponent }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DescargaRemesaRoutingModule { }
