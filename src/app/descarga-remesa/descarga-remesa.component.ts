import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { DescargaRemesaService } from "./descarga-remesa.service";

@Component({
  selector: 'app-descarga-remesa',
  templateUrl: './descarga-remesa.component.html',
  styleUrls: ['./descarga-remesa.component.scss']
})
export class DescargaRemesaComponent implements OnInit {
  
  formDescargaRemesa: FormGroup;
  empresas: any;
  loading = false;
  displayTable = false;
  success = false;
  error = false;
  errorMsg = '';
  errorMsgTable = 'Debe seleccionar un elemento';
  dataTable: any;
  selectRemesasTable: any | undefined;

  loadingDescarga = false;
  isSelectedTable = false;

  blob: any

  settings = {
    selectMode: 'multi',
    mode: 'external',
    hideSubHeader: true,
    editable: false,
    actions:{
      add:false,
      delete:false,
      edit:false,
      select: true,
    },
    columns: {
      folioRemesa: {
        title: 'Clave remesa',
        class: 'center'
      },
      fechaEmision: {
        title: 'Fecha de emisión'
      },
      horaEmision: {
        title: 'Hora de emisión'
      },
      estadoRemesa: {
        title: 'Estado de emisión'
      },
      cantDocumento: {
        title: 'Total Documentos'
      }
    }
  };

  @ViewChild('grdTags') grdTags: any;
  
  btnDescargaRemesas ()  {

    this.loadingDescarga = true;
    
    if(this.selectRemesasTable === undefined || this.selectRemesasTable.selected == 0){
      this.isSelectedTable = true;      
    }else{
      this.isSelectedTable = false;

      this.selectRemesasTable.selected.forEach(element => {
        
        this.descargaRemesaService.descargaRemesa(this.formDescargaRemesa.value, element ).subscribe(
          data => {
            this.blob = new Blob([data], {type: 'txt/plain'});
            console.log("element");
            console.log(element);
            var downloadURL = window.URL.createObjectURL(data);
            var link = document.createElement('a');
            link.href = downloadURL;
            link.download = element.folioRemesa + ".txt";
            link.click();
          },
          error => {
            console.log(`user error: ${error}`);
            // this.error = true;
            // // //TODO verificar undefined en mensaje
            // this.errorMsg = error.error.message;
          }
        );

      });
    }
  }

  onUserRowSelect(event) {
    this.selectRemesasTable = event;
    this.isSelectedTable = true;
    this.loadingDescarga = false;
  }

  constructor(private formBuilder: FormBuilder, 
    private descargaRemesaService: DescargaRemesaService) { }
  
  ngOnInit() {
    this.formDescargaRemesa = this.formBuilder.group({
      empresaId:["", Validators.required],
    });

    this.descargaRemesaService.getEmpresas().subscribe(data => {
      this.empresas = data;
    });
  }

  get empresa(){
    return this.formDescargaRemesa.get("empresaId");
  }

  onSubmitSearchRemesa() {
    this.loading = true;
    this.error = false;
    this.success = false;
    this.errorMsg= '';


    if (this.formDescargaRemesa.invalid) {
      return;
    }

    this.descargaRemesaService.getRemesasDatos(this.formDescargaRemesa.value).subscribe(
      data => {
        this.success = true;
        this.error = false;
        this.loading = false;
        this.displayTable = true;
        this.dataTable = data;
      
      },
      error => {
        console.log(`user error: ${error.error}`);
        this.error = true;
        this.displayTable = false;
        this.dataTable = [];
        // //TODO verificar undefined en mensaje
        this.errorMsg = error.error.message;
      });
  }

}
