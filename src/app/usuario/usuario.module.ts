import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioRoutingModule } from "./usuario-routing.module";
import { UsuarioComponent } from "./usuario.component";
import { ReactiveFormsModule } from '@angular/forms';
import { UsuarioService } from "./usuario.service";
import { FormsModule } from '@angular/forms';


import {
  NbThemeModule,
  NbCardModule,
  NbLayoutModule,
  NbSelectModule,
  NbInputModule,
  NbButtonModule,
  NbAlertModule,
  NbRadioModule
} from '@nebular/theme';


@NgModule({
  declarations: [UsuarioComponent],
  imports: [
    CommonModule,
    NbThemeModule,
    UsuarioRoutingModule,
    NbCardModule,
    NbLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    NbAlertModule,
    NbRadioModule
  ],
  providers:[ UsuarioService ]
})
export class UsuarioModule { }
