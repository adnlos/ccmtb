import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '@env/environment';

export interface UsuarioContext {
  email: string,
  empresaId: number,
  nombre: string,
  password: string,
  proveedorId: number,
  sucursalId: number,
  username: string
}

@Injectable()
export class UsuarioService {
  
  baseUrlPrivate = environment.serverUrlPrivate;
  constructor(private httpClient: HttpClient) { }
  
  getEmpresas(): Observable<any>{
    return this.httpClient.get<any>(this.baseUrlPrivate + 'empresas');
  }

  getProveedores(): Observable<any>{
    return this.httpClient.get<any>(this.baseUrlPrivate + 'proveedores')
  }

  getSucursales( empresaId: string):Observable<any>{
    const  params = new  HttpParams().set('empresaId', empresaId);
    return this.httpClient.get<any>(this.baseUrlPrivate + 'sucursales',{params});
  }

  saveUsuario(usuarioCtx: UsuarioContext):Observable<any>{
    return this.httpClient.post<any>(this.baseUrlPrivate + 'usuarios', usuarioCtx, {observe: 'response'} ).pipe(map(data => {
          return data;
      }));
  }

}
