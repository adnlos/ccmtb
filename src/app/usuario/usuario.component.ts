import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from  '@angular/forms';
import { UsuarioService } from "./usuario.service";

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {

  formUsuario: FormGroup;
  loading = false;
  success = false;
  error = false;
  tipousuario : boolean = true;
  errorMsg: string;
  empresas: any; 
  proveedores: any;
  sucursales: any;

  options = [
    { value: 'Proveedor', label: 'Proveedor' },
    { value: 'Empresa', label: 'Empresa' },
  ];

  constructor(private formBuilder: FormBuilder, private usuarioService :UsuarioService ) { }

  chageEmpresa(event) {
    this.usuarioService.getSucursales(event).subscribe(data => {
      this.sucursales = data;
    });
  }

  ngOnInit() {
    this.createForm();

    this.usuarioService.getEmpresas().subscribe(data => {
      this.empresas = data;
    });
    
    this.usuarioService.getProveedores().subscribe(data => {
      this.proveedores = data;
    });

    this.formUsuario.get('option').valueChanges.subscribe(value => { 
      if (value == 'Proveedor') {
        this.tipousuario = true;
        this.formUsuario.controls.proveedorId.setValidators(Validators.required);
        this.formUsuario.controls.empresaId.clearValidators()
        this.formUsuario.controls.sucursalId.clearValidators()
        this.formUsuario.controls.empresaId.setValue('');
        this.formUsuario.controls.sucursalId.setValue('');
      } else {
        this.tipousuario = false;
        this.formUsuario.controls.empresaId.setValidators(Validators.required);
        this.formUsuario.controls.sucursalId.setValidators(Validators.required);
        this.formUsuario.controls.proveedorId.clearValidators();
        this.formUsuario.controls.proveedorId.setValue('');
      }
    });
  }

  private createForm() {
    this.formUsuario = this.formBuilder.group({
      nombre:["", Validators.required],
      username:["", Validators.required],
      password:["", Validators.required],
      empresaId:[""],
      proveedorId:["", Validators.required],
      sucursalId:[""],
      email:["", [Validators.email, Validators.required]],
      option:[this.options[0].value]
    });
  }



  get nombre(){
    return this.formUsuario.get("nombre");
  }

  get email(){
    return this.formUsuario.get("email");
  }

  get password(){
    return this.formUsuario.get("password");
  }

  get username(){
    return this.formUsuario.get("username");
  }
  
  get empresa(){
    return this.formUsuario.get("empresaId");
  }

  get sucursal(){
    return this.formUsuario.get("sucursalId");
  }

  get proveedor(){
    return this.formUsuario.get("proveedorId");
  }

  onSubmit() {
    this.loading = true;
    this.error = false;
    this.success = false;
    this.errorMsg= '';


    if (this.formUsuario.invalid) {
      return;
    }

    this.usuarioService.saveUsuario(this.formUsuario.value).subscribe(
      data => {
      this.success = true;
      this.error = false;
      this.tipousuario = true;
      this.formUsuario.reset();
      this.formUsuario.controls.option.setValue(this.options[0].value);
    },
    error => {
      console.log(`user error: ${error.error.message}`);
      this.error = true;
      //TODO verificar undefined en mensaje
      this.errorMsg = error.error.message;
    }); 
  }
}
