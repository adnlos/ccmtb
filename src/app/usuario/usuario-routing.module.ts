import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioComponent } from './usuario.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'usuarios', component: UsuarioComponent }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsuarioRoutingModule { }
