import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CierreRemesaParcialComponent } from './cierre-remesa-parcial.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'cierre-remesa-parcial', component: CierreRemesaParcialComponent }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CierreRemesaParcialRoutingModule { }
