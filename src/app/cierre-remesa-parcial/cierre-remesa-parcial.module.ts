import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { CierreRemesaParcialRoutingModule } from "./cierre-remesa-parcial-routing.module";
import { CierreRemesaParcialComponent } from "./cierre-remesa-parcial.component";
import { CierreRemesaParcialService } from "./cierre-remesa-parcial.service";
import { Ng2SmartTableModule } from 'ng2-smart-table';


import {
  NbCardModule,
  NbSelectModule,
  NbInputModule,
  NbButtonModule,
  NbAlertModule,
  NbToastrService
} from '@nebular/theme';

@NgModule({
  declarations: [CierreRemesaParcialComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    NbAlertModule,
    CierreRemesaParcialRoutingModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
  ],
  providers:[CierreRemesaParcialService, NbToastrService]
})
export class CierreRemesaParcialModule { }
