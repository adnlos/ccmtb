import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CierreRemesaParcialComponent } from './cierre-remesa-parcial.component';

describe('CierreRemesaParcialComponent', () => {
  let component: CierreRemesaParcialComponent;
  let fixture: ComponentFixture<CierreRemesaParcialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CierreRemesaParcialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CierreRemesaParcialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
