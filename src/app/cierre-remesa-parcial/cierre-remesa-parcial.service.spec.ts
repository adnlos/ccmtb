import { TestBed } from '@angular/core/testing';

import { CierreRemesaParcialService } from './cierre-remesa-parcial.service';

describe('CierreRemesaParcialService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CierreRemesaParcialService = TestBed.get(CierreRemesaParcialService);
    expect(service).toBeTruthy();
  });
});
