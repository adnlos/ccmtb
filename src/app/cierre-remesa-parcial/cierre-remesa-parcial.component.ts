import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { CierreRemesaParcialService } from "./cierre-remesa-parcial.service";
import { NbToastrService } from '@nebular/theme';
import {LocalDataSource} from 'ng2-smart-table';


@Component({
  selector: 'app-cierre-remesa-parcial',
  templateUrl: './cierre-remesa-parcial.component.html',
  styleUrls: ['./cierre-remesa-parcial.component.scss']
})
export class CierreRemesaParcialComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, 
    private cierreRemesaParcialService: CierreRemesaParcialService, 
    private nbToastrService: NbToastrService) { }

  
  formCierreRemesaParcial: FormGroup;
  empresas: any;
  loadingSearch = false;
  displayTable = false;
  successSearch = false;
  errorSearch = false;
  errorMsgSearch = '';

  successRecepcion = false;
  localDataTableSource:  LocalDataSource;
  selectCierreParcialRemesasTable: any | undefined;

  isSelectedTable = false;
  loadingCierreParcial = false;



  
  ngOnInit() {
    this.formCierreRemesaParcial = this.formBuilder.group({
      empresaId:["", Validators.required],
    });

    this.cierreRemesaParcialService.getEmpresas().subscribe(data => {
      this.empresas = data;
    });
  }

  settings = {
    selectMode: 'multi',
    mode: 'external',
    hideSubHeader: true,
    editable: false,
    actions:{
      add:false,
      delete:false,
      edit:false,
      select: true,
    },
    columns: {
      folioRemesa: {
        title: 'Clave remesa',
        class: 'center'
      },
      fechaEmision: {
        title: 'Fecha de emisión'
      },
      horaEmision: {
        title: 'Hora de emisión'
      },
      estadoRemesa: {
        title: 'Estado de emisión'
      },
      cantDocumento: {
        title: 'Total Documentos'
      }
    }
  };

  get empresa(){
    return this.formCierreRemesaParcial.get("empresaId");
  }


  buscarCierreParcialRemesa(){

    this.loadingSearch = true;
    this.errorSearch = false;
    this.successSearch = false;
    this.errorMsgSearch= '';
    this.successRecepcion = false;

    if (this.formCierreRemesaParcial.invalid) {
      return;
    }

    this.cierreRemesaParcialService.getRemesasDatos(this.formCierreRemesaParcial.value).subscribe(
      data => {
        this.successSearch = true;
        this.errorSearch = false;
        this.loadingSearch = false;
        this.displayTable = true;
        this.localDataTableSource = new LocalDataSource(data);
      },
      error => {
        console.log(error);
        this.errorSearch = true;
        this.displayTable = false;
        this.localDataTableSource = new LocalDataSource([]);
        // //TODO verificar undefined en mensaje
        this.errorMsgSearch = error.error.message;
      });
  }




  btnCierreParcialRemesa(){
    console.log("Cierre Parcial");
    this.loadingCierreParcial = true;
    this.successRecepcion = false;
    
    if(this.selectCierreParcialRemesasTable === undefined || this.selectCierreParcialRemesasTable.selected == 0){
      this.isSelectedTable = true;      
    }else{
      this.isSelectedTable = false;
      let arrayRemesas = this.selectCierreParcialRemesasTable.selected.map(element => element.folioRemesa).join(', ')
      
      this.cierreRemesaParcialService.setRemesasCierreParcial(this.formCierreRemesaParcial.value, arrayRemesas).subscribe(
        data =>{
          console.log(data);
          this.successRecepcion = true;
          this.nbToastrService.success('Exito!', 'Remesas Actualizadas');
          this.removeElementsTable();
        },
        error => {
          console.log(error);
          this.nbToastrService.danger('Error', 'Error al recepcionar remesas');
        }
      );
    }
  }



  private removeElementsTable(){
    console.log("remove elements");
    this.selectCierreParcialRemesasTable.selected.forEach(element => {
      this.localDataTableSource.remove(element);
    });

    if (this.localDataTableSource.count() == 0) {
      this.displayTable = false;
      this.nbToastrService.info('Exito!', 'No hay mas registros');
      
    } else {
      
    }

  }

  onUserRowSelect(event) {
    this.selectCierreParcialRemesasTable = event;
    this.isSelectedTable = true;
    this.loadingCierreParcial = false;
    this.successRecepcion = false;
  }



}
