import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../authentication/authentication.service'


@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private injector: Injector){}
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        const auth = this.injector.get(AuthenticationService);
        let credentials = auth.credentials;

        if(credentials != null){
            request = request.clone({
                setHeaders: { 
                    'X-Auth-Token': `${credentials.token}` ,
                    'Content-Type': 'application/json', 
                    'Accept': 'application/json'
                }
            });
        }
        return next.handle(request);
    }
}