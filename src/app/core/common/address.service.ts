import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse  } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';

import { environment } from '@env/environment';

export interface Paises {
  
}

@Injectable()
export class AddressService {
  baseUrl = environment.serverUrlPrivate;

  constructor(private http: HttpClient ) { 
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'

    })
  };

  getPaises(): Observable<any> {
    return this.http.get('${baseUrl}/paises').pipe(
      map(this.extractData));
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  



  
}
