import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '@env/environment';

export interface Credentials {
  id: string;
  username: string;
  nombre: string;
  empresaId: string;
  sucursalId: string;
  proveedorlId: string;
  roles: { authority: string }[];
  email: string;
  modulos: funcionesMenu[];
  token: string;
}

export interface funcionesMenu{
  modulo: string;
  funciones: {funcion: string, privilegio:[]}
}


export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

const credentialsKey = 'credentials_ccmtb';

/**
 * Provides a base for authentication workflow.
 * The Credentials interface as well as login/logout methods should be replaced with proper implementation.
 */
@Injectable()
export class AuthenticationService {
  private _credentials: Credentials | null;

  baseUrl = environment.serverUrlPublic;
  baseUrlPrivate = environment.serverUrlPrivate;


  constructor(private httpClient: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Observable<Credentials> {
  
    return this.httpClient.post<any>(this.baseUrl + 'login', context , {
        headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' }
      }).pipe(map(user => {
          // login successful if there's a jwt token in the response
          if (user && user.token) {
            this.setCredentials(user, context.remember);
          }
          return user;
      }));
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    this.httpClient.post<any>(this.baseUrlPrivate + 'logout', null);
    this.setCredentials();
    return of(true);
  }

  /**
   * Checks is the user is authenticated.
   * @return True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  async isTokenValid() : Promise<any> {
    let asyncResult = await this.httpClient.post<any>(this.baseUrlPrivate + 'valid/token', null).toPromise();
    return asyncResult;
  }
  /**
   * Gets the user credentials.
   * @return The user credentials or null if the user is not authenticated.
   */
  get credentials(): Credentials | null {
    return this._credentials;
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param credentials The user credentials.
   * @param remember True to remember credentials across sessions.
   */
  private setCredentials(credentials?: Credentials, remember?: boolean) {
    this._credentials = credentials || null;

    if (credentials) {
      const storage = remember ? localStorage : sessionStorage;
      storage.setItem(credentialsKey, JSON.stringify(credentials));
    } else {
      sessionStorage.removeItem(credentialsKey);
      localStorage.removeItem(credentialsKey);
    }
  }
}
