import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CargaInventarioComponent } from './carga-inventario.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'carga-inventario', component: CargaInventarioComponent }])
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CargaInventarioRoutingModule { }
