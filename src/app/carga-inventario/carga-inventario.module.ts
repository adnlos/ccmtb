import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { CargaInventarioRoutingModule } from "./carga-inventario-routing.module";
import { CargaInventarioComponent } from "./carga-inventario.component";
import { CargaInventarioService } from "./carga-inventario.service";
import { Ng2SmartTableModule } from 'ng2-smart-table';


import {
  NbCardModule,
  NbSelectModule,
  NbInputModule,
  NbButtonModule,
  NbAlertModule,
  NbToastrService
} from '@nebular/theme';


@NgModule({
  declarations: [CargaInventarioComponent],
  imports: [
    CommonModule,
    CargaInventarioRoutingModule,
    NbCardModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    NbAlertModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
  ],
  providers:[CargaInventarioService, NbToastrService]
})
export class CargaInventarioModule { }
