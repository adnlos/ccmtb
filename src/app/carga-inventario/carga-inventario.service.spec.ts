import { TestBed } from '@angular/core/testing';

import { CargaInventarioService } from './carga-inventario.service';

describe('CargaInventarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CargaInventarioService = TestBed.get(CargaInventarioService);
    expect(service).toBeTruthy();
  });
});
