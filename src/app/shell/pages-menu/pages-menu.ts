import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Inicio',
    icon: 'nb-person',
    link: '/pages',
    home: true,
  },
  {
    title: 'Carga',
    group: true,
  },

  {
    title: 'Carga de inventario',
    icon: 'nb-star',
    children: [
      {
        title: 'Carga semi-automática',
        link: '#',
      }
    ],
  },

  {
    title: 'Procesamiento',
    group: true,
  },
  
  {
    title: 'Procesamiento',
    icon: 'nb-compose',
    children: [
      {
        title: 'Procesamiento de remesas',
        link: '',
      },
      {
        title: 'Procesamiento de agilizaciones y quejas',
        link: '',
      }
    ],
  },

  {
    title: 'Bóveda',
    group: true,
  },

  {
    title: 'Bóveda',
    icon: 'nb-compose',
    children: [
      {
        title: 'Consulta',
        link: '',
      }
    ],
  },

  {
    title: 'Usuarios',
    group: true,
  },

  {
    title: 'Nuevo',
    icon: 'nb-compose',
    link: '/usuario/nuevo',    
  },


];
