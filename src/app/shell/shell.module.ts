import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ShellComponent } from './shell.component';
import { HeaderComponent } from './header/header.component';
import { NbThemeModule, NbLayoutModule, 
  NbMenuModule, NbSidebarModule, 
  NbActionsModule, NbUserModule, NbContextMenuModule } from '@nebular/theme';
import { PagesMenuComponent } from './pages-menu/pages-menu.component';
import { LayoutService } from './layout.service';
import { MenuService } from './menu.service';

@NgModule({
  imports: [CommonModule, TranslateModule, 
    NgbModule, RouterModule, NbThemeModule, 
    NbLayoutModule, NbMenuModule, 
    NbSidebarModule, NbActionsModule,
    NbUserModule, NbContextMenuModule
  ],
  providers: [LayoutService, MenuService],
  declarations: [HeaderComponent, ShellComponent, PagesMenuComponent ]
})
export class ShellModule {}
