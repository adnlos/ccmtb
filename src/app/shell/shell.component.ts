import { Component, OnDestroy, OnInit } from '@angular/core';
import { delay, withLatestFrom, takeWhile } from 'rxjs/operators';
import {
  NbMediaBreakpoint,
  NbMediaBreakpointsService,
  NbMenuItem,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';

import { MenuService } from './menu.service';

import { MENU_ITEMS } from './pages-menu/pages-menu';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnDestroy, OnInit {

  menu: NbMenuItem[] = [
    {
      title: 'Inicio',
      icon: 'nb-home',
      link: '/',
      home: false,
    },
  ];


  layout: any = {};
  sidebar: any = {};

  private alive = true;

  currentTheme: string;

  constructor(protected nbMenuService: NbMenuService,
              protected themeService: NbThemeService,
              protected bpService: NbMediaBreakpointsService,
              protected sidebarService: NbSidebarService,
              private menuService: MenuService) {

    const isBp = this.bpService.getByName('is');
    this.nbMenuService.onItemSelect()
      .pipe(
        takeWhile(() => this.alive),
        withLatestFrom(this.themeService.onMediaQueryChange()),
        delay(20),
      )
      .subscribe(([item, [bpFrom, bpTo]]: [any, [NbMediaBreakpoint, NbMediaBreakpoint]]) => {

        if (bpTo.width <= isBp.width) {
          this.sidebarService.collapse('menu-sidebar');
        }
      });

    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.currentTheme = theme.name;
    });
    
    let resMenu = menuService.getMenu().subscribe(data => {
      data.forEach(element => {
        let children: any[] = [];
        element.funciones.forEach( funcion => {
          children.push({title: funcion.funcion, link: '/'+funcion.url});
        });
        var menuItem: NbMenuItem = {title: element.modulo, icon: element.icono, children: children};
        this.menu.push(menuItem);
      });
    });
  }


  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
