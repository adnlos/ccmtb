import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
// import 'rxjs/add/operator/map'

import { HttpClient, HttpHeaders } from '@angular/common/http';


import { environment } from '@env/environment';

export interface MenuModel {
  modulo: string;
  funciones: funcionItem[];
  
}

export interface funcionItem {
  funcion: string;
  privilegios: {privilegio:[]};
  url: string;
}

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  baseUrlPrivate = environment.serverUrlPrivate;
  constructor(private httpClient: HttpClient) { }
  
  getMenu(): Observable<any>{
    return this.httpClient.get<any>(this.baseUrlPrivate + 'menu');
  }

}
