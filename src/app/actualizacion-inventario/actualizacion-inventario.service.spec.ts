import { TestBed } from '@angular/core/testing';

import { ActualizacionInventarioService } from './actualizacion-inventario.service';

describe('ActualizacionInventarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActualizacionInventarioService = TestBed.get(ActualizacionInventarioService);
    expect(service).toBeTruthy();
  });
});
