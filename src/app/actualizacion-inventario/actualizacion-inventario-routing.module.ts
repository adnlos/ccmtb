import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActualizacionInventarioComponent } from './actualizacion-inventario.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'actualizacion-inventario', component: ActualizacionInventarioComponent }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActualizacionInventarioRoutingModule { }
