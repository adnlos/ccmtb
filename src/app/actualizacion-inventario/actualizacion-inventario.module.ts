import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { ActualizacionInventarioRoutingModule } from "./actualizacion-inventario-routing.module";
import { ActualizacionInventarioComponent } from "./actualizacion-inventario.component";
import { ActualizacionInventarioService } from "./actualizacion-inventario.service";
import { Ng2SmartTableModule } from 'ng2-smart-table';


import {
  NbCardModule,
  NbSelectModule,
  NbInputModule,
  NbButtonModule,
  NbAlertModule,
  NbToastrService
} from '@nebular/theme';


@NgModule({
  declarations: [ActualizacionInventarioComponent],
  imports: [
    CommonModule,
    ActualizacionInventarioRoutingModule,
    ReactiveFormsModule,
    Ng2SmartTableModule
  ],
  providers:[ActualizacionInventarioService]
})
export class ActualizacionInventarioModule { }
