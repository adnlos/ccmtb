import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { CierreRemesaRealRoutingModule } from "./cierre-remesa-real-routing.module";
import { CierreRemesaRealComponent } from "./cierre-remesa-real.component";
import { CierreRemesaRealService } from "./cierre-remesa-real.service";
import { Ng2SmartTableModule } from 'ng2-smart-table';


import {
  NbCardModule,
  NbSelectModule,
  NbInputModule,
  NbButtonModule,
  NbAlertModule,
  NbToastrService
} from '@nebular/theme';

@NgModule({
  declarations: [CierreRemesaRealComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    NbAlertModule,
    CierreRemesaRealRoutingModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
  ],
  providers:[CierreRemesaRealService, NbToastrService]
})
export class CierreRemesaRealModule { }
