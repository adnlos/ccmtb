import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { CierreRemesaRealService } from "./cierre-remesa-real.service";
import { NbToastrService } from '@nebular/theme';
import {LocalDataSource} from 'ng2-smart-table';


@Component({
  selector: 'app-cierre-remesa-real',
  templateUrl: './cierre-remesa-real.component.html',
  styleUrls: ['./cierre-remesa-real.component.scss']
})
export class CierreRemesaRealComponent implements OnInit {


  constructor(private formBuilder: FormBuilder, 
    private cierreRemesaRealService: CierreRemesaRealService, 
    private nbToastrService: NbToastrService) { }

  
  formCierreRemesaReal: FormGroup;
  empresas: any;
  loadingSearch = false;
  displayTable = false;
  successSearch = false;
  errorSearch = false;
  errorMsgSearch = '';

  successRecepcion = false;
  localDataTableSource:  LocalDataSource;
  selectRemesasTable: any | undefined;

  isSelectedTable = false;
  loadingRemesaReal = false;

  
  ngOnInit() {
    this.formCierreRemesaReal = this.formBuilder.group({
      empresaId:["", Validators.required],
    });

    this.cierreRemesaRealService.getEmpresas().subscribe(data => {
      this.empresas = data;
    });
  }

  settings = {
    selectMode: 'multi',
    mode: 'external',
    hideSubHeader: true,
    editable: false,
    actions:{
      add:false,
      delete:false,
      edit:false,
      select: true,
    },
    columns: {
      folioRemesa: {
        title: 'Clave remesa',
        class: 'center'
      },
      fechaEmision: {
        title: 'Fecha de emisión'
      },
      horaEmision: {
        title: 'Hora de emisión'
      },
      estadoRemesa: {
        title: 'Estado de emisión'
      },
      cantDocumento: {
        title: 'Total Documentos'
      }
    }
  };

  get empresa(){
    return this.formCierreRemesaReal.get("empresaId");
  }


  buscarCierreRemesaReal(){
    this.loadingSearch = true;
    this.errorSearch = false;
    this.successSearch = false;
    this.errorMsgSearch= '';
    this.successRecepcion = false;

    if (this.formCierreRemesaReal.invalid) {
      return;
    }

    this.cierreRemesaRealService.getRemesasDatos(this.formCierreRemesaReal.value).subscribe(
      data => {
        this.successSearch = true;
        this.errorSearch = false;
        this.loadingSearch = false;
        this.displayTable = true;
        this.localDataTableSource = new LocalDataSource(data);
      },
      error => {
        console.log(error);
        this.errorSearch = true;
        this.displayTable = false;
        this.localDataTableSource = new LocalDataSource([]);
        // //TODO verificar undefined en mensaje
        this.errorMsgSearch = error.error.message;
      });
  }


  btnCierreRealRemesa(){
    console.log("recepcion de remesas");
    this.loadingRemesaReal = true;
    this.successRecepcion = false;
    
    if(this.selectRemesasTable === undefined || this.selectRemesasTable.selected == 0){
      this.isSelectedTable = true;      
    }else{
      this.isSelectedTable = false;
      let arrayRemesas = this.selectRemesasTable.selected.map(element => element.folioRemesa).join(', ')
      
      this.cierreRemesaRealService.setRemesasCierreBoveda(this.formCierreRemesaReal.value, arrayRemesas).subscribe(
        data =>{
          console.log(data);
          this.successRecepcion = true;
          this.nbToastrService.success('Exito!', 'Remesas Actualizadas');
          this.removeElementsTable();
        },
        error => {
          console.log(error);
          this.nbToastrService.danger('Error', 'Error al recepcionar remesas');
        }
      );
    }
  }



  private removeElementsTable(){
    console.log("remove elements");
    this.selectRemesasTable.selected.forEach(element => {
      this.localDataTableSource.remove(element);
    });

    if (this.localDataTableSource.count() == 0) {
      this.displayTable = false;
      this.nbToastrService.info('Exito!', 'No hay mas registros');
      
    } else {
      
    }
    // this.localDataTableSource.refresh();

  }

  onUserRowSelect(event) {
    this.selectRemesasTable = event;
    this.isSelectedTable = true;
    this.loadingRemesaReal = false;
    this.successRecepcion = false;
  }



}
