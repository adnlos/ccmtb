import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CierreRemesaRealComponent } from './cierre-remesa-real.component';

describe('CierreRemesaRealComponent', () => {
  let component: CierreRemesaRealComponent;
  let fixture: ComponentFixture<CierreRemesaRealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CierreRemesaRealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CierreRemesaRealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
