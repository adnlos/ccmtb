import { TestBed } from '@angular/core/testing';

import { CierreRemesaRealService } from './cierre-remesa-real.service';

describe('CierreRemesaRealService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CierreRemesaRealService = TestBed.get(CierreRemesaRealService);
    expect(service).toBeTruthy();
  });
});
