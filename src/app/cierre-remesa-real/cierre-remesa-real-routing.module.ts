import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CierreRemesaRealComponent } from './cierre-remesa-real.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'cierre-remesa-real', component: CierreRemesaRealComponent }])
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CierreRemesaRealRoutingModule { }
