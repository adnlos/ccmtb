import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultaInventarioComponent } from './consulta-inventario.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'consulta-inventario', component: ConsultaInventarioComponent}])
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultaInventarioRoutingModule { }
