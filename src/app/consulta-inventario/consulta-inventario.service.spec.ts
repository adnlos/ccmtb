import { TestBed } from '@angular/core/testing';

import { ConsultaInventarioService } from './consulta-inventario.service';

describe('ConsultaInventarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConsultaInventarioService = TestBed.get(ConsultaInventarioService);
    expect(service).toBeTruthy();
  });
});
