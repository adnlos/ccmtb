import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RecepcionRemesaRoutingModule } from "./recepcion-remesa-routing.module";
import { RecepcionRemesaComponent } from "./recepcion-remesa.component";
import { RecepcionRemesaService } from "./recepcion-remesa.service";
import { Ng2SmartTableModule } from 'ng2-smart-table';


import {
  NbThemeModule,
  NbCardModule,
  NbLayoutModule,
  NbSelectModule,
  NbInputModule,
  NbButtonModule,
  NbAlertModule,
  NbCalendarModule,
  NbDatepickerModule,
  NbToastrService
} from '@nebular/theme';



@NgModule({
  declarations: [RecepcionRemesaComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RecepcionRemesaRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule
  ],
  providers:[RecepcionRemesaService, NbToastrService]
})
export class RecepcionRemesaModule { }
