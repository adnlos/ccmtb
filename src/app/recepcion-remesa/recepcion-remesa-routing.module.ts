import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecepcionRemesaComponent } from './recepcion-remesa.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'recepcion-remesa', component: RecepcionRemesaComponent }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecepcionRemesaRoutingModule { }
