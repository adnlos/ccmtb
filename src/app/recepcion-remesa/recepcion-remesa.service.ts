import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';

import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';


@Injectable({
  providedIn: 'root'
})
export class RecepcionRemesaService {


  baseUrlPrivate = environment.serverUrlPrivate;
  constructor(private httpClient: HttpClient) { }
  
  getEmpresas(): Observable<any>{
    return this.httpClient.get<any>(this.baseUrlPrivate + 'empresas');
  }

  getRemesasDatos( request: any):Observable<any>{
    const  params = new  HttpParams().set('empresaId', request.empresaId);
    return this.httpClient.get<any>(this.baseUrlPrivate + 'remesas/recepcion/consulta/',{params});
  }

  setRemesasRecepcion(empresa: any, remesasArray:any):Observable<any>{
    let params = new  HttpParams().set('empresaId', empresa.empresaId).set('remesas', remesasArray);
    let options = {empresaId: 1};

    return this.httpClient.put(this.baseUrlPrivate + 'remesas/recepcion/', options, {params:params});        
  }

  private getArgHeaders(): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      })
    };
    return httpOptions;
  }

}
