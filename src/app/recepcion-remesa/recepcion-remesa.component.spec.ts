import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecepcionRemesaComponent } from './recepcion-remesa.component';

describe('RecepcionRemesaComponent', () => {
  let component: RecepcionRemesaComponent;
  let fixture: ComponentFixture<RecepcionRemesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecepcionRemesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecepcionRemesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
