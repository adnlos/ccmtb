import { TestBed } from '@angular/core/testing';

import { RecepcionRemesaService } from './recepcion-remesa.service';

describe('RecepcionRemesaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecepcionRemesaService = TestBed.get(RecepcionRemesaService);
    expect(service).toBeTruthy();
  });
});
