import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { RecepcionRemesaService } from "./recepcion-remesa.service";
import { NbToastrService } from '@nebular/theme';
import {LocalDataSource} from 'ng2-smart-table';


@Component({
  selector: 'app-recepcion-remesa',
  templateUrl: './recepcion-remesa.component.html',
  styleUrls: ['./recepcion-remesa.component.scss']
})
export class RecepcionRemesaComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, 
    private recepcionRemesaService: RecepcionRemesaService, private nbToastrService: NbToastrService) { }

  formRecepcionRemesa: FormGroup;
  empresas: any;
  loading = false;
  displayTable = false;
  success = false;
  successRecepcion = false;
  error = false;
  errorMsg = '';

  errorMsgTable = 'Debe seleccionar un elemento';
  dataTable: any;
  localDataTableSource:  LocalDataSource;
  selectRemesasTable: any | undefined;

  isSelectedTable = false;
  loadingRecepcion = false;

  @ViewChild('grdTags') grdTags: any;

  ngOnInit() {
    this.formRecepcionRemesa = this.formBuilder.group({
      empresaId:["", Validators.required],
    });

    this.recepcionRemesaService.getEmpresas().subscribe(data => {
      this.empresas = data;
    });
  }
  
  settings = {
    selectMode: 'multi',
    mode: 'external',
    hideSubHeader: true,
    editable: false,
    actions:{
      add:false,
      delete:false,
      edit:false,
      select: true,
    },
    columns: {
      folioRemesa: {
        title: 'Clave remesa',
        class: 'center'
      },
      fechaEmision: {
        title: 'Fecha de emisión'
      },
      horaEmision: {
        title: 'Hora de emisión'
      },
      estadoRemesa: {
        title: 'Estado de emisión'
      },
      cantDocumento: {
        title: 'Total Documentos'
      }
    }
  };


  get empresa(){
    return this.formRecepcionRemesa.get("empresaId");
  }

  buscarRecepcionRemesa(){

    this.loading = true;
    this.error = false;
    this.success = false;
    this.errorMsg= '';
    this.successRecepcion = false;

    if (this.formRecepcionRemesa.invalid) {
      return;
    }

    this.recepcionRemesaService.getRemesasDatos(this.formRecepcionRemesa.value).subscribe(
      data => {
        this.success = true;
        this.error = false;
        this.loading = false;
        this.displayTable = true;
        this.dataTable = data;
        this.localDataTableSource = new LocalDataSource(data);
      },
      error => {
        console.log(error);
        this.error = true;
        this.displayTable = false;
        this.dataTable = [];
        this.localDataTableSource = new LocalDataSource([]);
        // //TODO verificar undefined en mensaje
        this.errorMsg = error.error.message;
      });
    
  }

  btnRecepcionarRemesas(){
    console.log("recepcion de remesas");
    this.loadingRecepcion = true;
    this.successRecepcion = false;

    console.log(this.grdTags);
    console.log(this.dataTable);
    // console.log(this.dataTable.pop(this.selectRemesasTable.selected[0]));

    if(this.selectRemesasTable === undefined || this.selectRemesasTable.selected == 0){
      this.isSelectedTable = true;      
    }else{
      this.isSelectedTable = false;
      let arrayRemesas = this.selectRemesasTable.selected.map(element => element.folioRemesa).join(', ')
      
      this.recepcionRemesaService.setRemesasRecepcion(this.formRecepcionRemesa.value, arrayRemesas).subscribe(
        data =>{
          console.log(data);
          this.successRecepcion = true;
          this.nbToastrService.success('Exito!', 'Remesas Actualizadas');
          this.removeElementsTable();
        },
        error => {
          console.log(error);
          this.nbToastrService.danger('Error', 'Error al recepcionar remesas');
        }
      );
    }
  }

  private removeElementsTable(){
    console.log("remove elements");
    this.selectRemesasTable.selected.forEach(element => {
      this.localDataTableSource.remove(element);
    });

    if (this.localDataTableSource.count() == 0) {
      this.displayTable = false;
      this.nbToastrService.info('Exito!', 'No hay mas registros');
      
    } else {
      
    }
    // this.localDataTableSource.refresh();

  }

  onUserRowSelect(event) {
    this.selectRemesasTable = event;
    this.isSelectedTable = true;
    this.loadingRecepcion = false;
    this.successRecepcion = false;
  }

}
