import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Shell } from '@app/shell/shell.service';
import { EmpleadoComponent } from './empleado.component';
import {NuevoComponent} from './nuevo/nuevo.component';

const routes: Routes = [
  Shell.childRoutes(
    [
      { 
        path: 'empleado', 
        component: EmpleadoComponent,
        children:[
          {
            path: 'nuevo',
            component: NuevoComponent
          }
        ] 
      }
    ]
  )
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class EmpleadoRoutingModule { }
