import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NuevoComponent } from './nuevo/nuevo.component';
import { EmpleadoComponent } from './empleado.component';
import {
  NbThemeModule,
  NbCardModule,
  NbCheckboxModule
} from '@nebular/theme';

import {EmpleadoRoutingModule} from './empleado-routing.module';

@NgModule({
  declarations: [EmpleadoComponent, NuevoComponent],
  imports: [
    NbThemeModule,
    CommonModule,
    EmpleadoRoutingModule,
    NbCardModule,
    NbCheckboxModule
  ]
})
export class EmpleadoModule { }
