import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { CierreRemesaBovedaService } from "./cierre-remesa-boveda.service";
import { NbToastrService } from '@nebular/theme';
import {LocalDataSource} from 'ng2-smart-table';

@Component({
  selector: 'app-cierre-remesa-boveda',
  templateUrl: './cierre-remesa-boveda.component.html',
  styleUrls: ['./cierre-remesa-boveda.component.scss']
})
export class CierreRemesaBovedaComponent implements OnInit {



  constructor(private formBuilder: FormBuilder, 
    private cierreRemesaBovedaService: CierreRemesaBovedaService, 
    private nbToastrService: NbToastrService) { }

  
  formCierreRemesaBoveda: FormGroup;
  empresas: any;
  loadingSearch = false;
  displayTable = false;
  successSearch = false;
  errorSearch = false;
  errorMsgSearch = '';

  successRecepcion = false;
  localDataTableSource:  LocalDataSource;
  selectCierreBovedaRemesasTable: any | undefined;

  
  isSelectedTable = false;
  loadingCierreBovedaRemesa = false;


  
  ngOnInit() {
    this.formCierreRemesaBoveda = this.formBuilder.group({
      empresaId:["", Validators.required],
    });

    this.cierreRemesaBovedaService.getEmpresas().subscribe(data => {
      this.empresas = data;
    });
  }

  settings = {
    selectMode: 'multi',
    mode: 'external',
    hideSubHeader: true,
    editable: false,
    actions:{
      add:false,
      delete:false,
      edit:false,
      select: true,
    },
    columns: {
      folioRemesa: {
        title: 'Clave remesa',
        class: 'center'
      },
      fechaEmision: {
        title: 'Fecha de emisión'
      },
      horaEmision: {
        title: 'Hora de emisión'
      },
      estadoRemesa: {
        title: 'Estado de emisión'
      },
      cantDocumento: {
        title: 'Total Documentos'
      }
    }
  };

  get empresa(){
    return this.formCierreRemesaBoveda.get("empresaId");
  }


  buscarCierreRemesaBoveda(){

    this.loadingSearch = true;
    this.errorSearch = false;
    this.successSearch = false;
    this.errorMsgSearch= '';
    this.successRecepcion = false;

    if (this.formCierreRemesaBoveda.invalid) {
      return;
    }

    this.cierreRemesaBovedaService.getRemesasDatos(this.formCierreRemesaBoveda.value).subscribe(
      data => {
        this.successSearch = true;
        this.errorSearch = false;
        this.loadingSearch = false;
        this.displayTable = true;
        this.localDataTableSource = new LocalDataSource(data);
      },
      error => {
        console.log(error);
        this.errorSearch = true;
        this.displayTable = false;
        this.localDataTableSource = new LocalDataSource([]);
        // //TODO verificar undefined en mensaje
        this.errorMsgSearch = error.error.message;
      });
  }



  btnCierreBovedaRemesa(){
    console.log("recepcion de remesas");
    this.loadingCierreBovedaRemesa = true;
    this.successRecepcion = false;
    
    if(this.selectCierreBovedaRemesasTable === undefined || this.selectCierreBovedaRemesasTable.selected == 0){
      this.isSelectedTable = true;      
    }else{
      this.isSelectedTable = false;
      let arrayRemesas = this.selectCierreBovedaRemesasTable.selected.map(element => element.folioRemesa).join(', ')
      
      this.cierreRemesaBovedaService.setRemesasCierreBoveda(this.formCierreRemesaBoveda.value, arrayRemesas).subscribe(
        data =>{
          console.log(data);
          this.successRecepcion = true;
          this.nbToastrService.success('Exito!', 'Remesas Actualizadas');
          this.removeElementsTable();
        },
        error => {
          console.log(error);
          this.nbToastrService.danger('Error', 'Error al recepcionar remesas');
        }
      );
    }
  }



  private removeElementsTable(){
    console.log("remove elements");
    this.selectCierreBovedaRemesasTable.selected.forEach(element => {
      this.localDataTableSource.remove(element);
    });

    if (this.localDataTableSource.count() == 0) {
      this.displayTable = false;
      this.nbToastrService.info('Exito!', 'No hay mas registros');
      
    } else {
      
    }

  }

  onUserRowSelect(event) {
    this.selectCierreBovedaRemesasTable = event;
    this.isSelectedTable = true;
    this.loadingCierreBovedaRemesa = false;
    this.successRecepcion = false;
  }


}
