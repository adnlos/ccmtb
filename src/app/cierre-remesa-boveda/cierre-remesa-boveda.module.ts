import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { CierreRemesaBovedaRoutingModule } from "./cierre-remesa-boveda-routing.module";
import { CierreRemesaBovedaComponent } from "./cierre-remesa-boveda.component";
import { CierreRemesaBovedaService } from "./cierre-remesa-boveda.service";
import { Ng2SmartTableModule } from 'ng2-smart-table';


import {
  NbCardModule,
  NbSelectModule,
  NbInputModule,
  NbButtonModule,
  NbAlertModule,
  NbToastrService
} from '@nebular/theme';

@NgModule({
  declarations: [CierreRemesaBovedaComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    NbAlertModule,
    CierreRemesaBovedaRoutingModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
  ],
  providers:[CierreRemesaBovedaService, NbToastrService]
})
export class CierreRemesaBovedaModule { }
