import { TestBed } from '@angular/core/testing';

import { CierreRemesaBovedaService } from './cierre-remesa-boveda.service';

describe('CierreRemesaBovedaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CierreRemesaBovedaService = TestBed.get(CierreRemesaBovedaService);
    expect(service).toBeTruthy();
  });
});
