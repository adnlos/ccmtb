import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CierreRemesaBovedaComponent } from './cierre-remesa-boveda.component';

describe('CierreRemesaBovedaComponent', () => {
  let component: CierreRemesaBovedaComponent;
  let fixture: ComponentFixture<CierreRemesaBovedaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CierreRemesaBovedaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CierreRemesaBovedaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
