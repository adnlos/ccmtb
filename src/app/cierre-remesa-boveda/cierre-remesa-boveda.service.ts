import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class CierreRemesaBovedaService {


  baseUrlPrivate = environment.serverUrlPrivate;
  constructor(private httpClient: HttpClient) { }
  
  getEmpresas(): Observable<any>{
    return this.httpClient.get<any>(this.baseUrlPrivate + 'empresas');
  }

  getRemesasDatos( request: any):Observable<any>{
    const  params = new  HttpParams().set('empresaId', request.empresaId);
    return this.httpClient.get<any>(this.baseUrlPrivate + 'remesas/cierreBoveda/consulta/',{params});
    // return this.httpClient.get<any>(this.baseUrlPrivate + 'remesas/recepcion/consulta/',{params});
  }

  setRemesasCierreBoveda(empresa: any, remesasArray:any):Observable<any>{
    let  params = new  HttpParams().set('empresaId', empresa.empresaId).set('remesas', remesasArray);
    let options = {empresaId: 1};

    // return this.httpClient.put(this.baseUrlPrivate + 'remesas/recepcion/', options, {params:params});        
    return this.httpClient.put(this.baseUrlPrivate + 'remesas/cierreBoveda/', options, {params:params});        
  }
}

// /api/v1/private/remesas/cierreBoveda/ 
// /api/v1/private/remesas/cierreBoveda/consulta