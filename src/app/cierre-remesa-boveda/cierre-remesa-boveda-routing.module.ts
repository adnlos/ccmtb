import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CierreRemesaBovedaComponent } from './cierre-remesa-boveda.component';
import { Shell } from "../shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([{ path: 'cierre-remesa-boveda', component: CierreRemesaBovedaComponent }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CierreRemesaBovedaRoutingModule { }
