import env from './.env';

export const environment = {
  production: true,
  version: env.npm_package_version,
  serverUrl: 'http://localhost:8090/api/v1/',
  serverUrlPublic: 'http://localhost:8090/api/v1/public/',
  serverUrlPrivate:'http://localhost:8090/api/v1/private/',
  defaultLanguage: 'en-US',
  supportedLanguages: ['en-US', 'es-ES']
};
